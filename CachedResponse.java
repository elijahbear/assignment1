import java.util.*;
import java.text.*;
import java.io.*;
import java.nio.file.Files;
import java.lang.String;
import java.lang.Byte;
import java.lang.Character;
import java.lang.StringBuffer;

public class CachedResponse implements java.io.Serializable //so we can store it in the cachemap which gets put in a file
{
  public Date date = null;
  public long maxage = -1;
  public Date expiration = null;
  public File response;

  public CachedResponse(File resp)
  {
    byte[] buffer = new byte[500];
    try
    {
      FileInputStream in = new FileInputStream(resp);
      in.read(buffer);
      in.close();
    }
    catch(IOException e)
    {
      e.printStackTrace();
    }

    DateFormat datemat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");

    String line;
    int lineCount = 0;
    List<String> headerlist = Arrays.asList(new String(getHeaderBytes(buffer)).split("[\n]"));
    //this really long loop searches through the header lines looking for Date, Expiration, and max-age
    while(lineCount < headerlist.size())
    {
      line = headerlist.get(lineCount).trim();
      if((line).matches(".*Date:.*"))
      {
        try
        {
          date = datemat.parse(line.substring(6));
        }
        catch(ParseException pe)
        {
          pe.printStackTrace();
          date = null;
        }
      }
      else
      {
        if(line.matches(".*Expires:.*"))
        {
          try
          {
            expiration = datemat.parse(line.substring(9));
          }
          catch(ParseException pe)
          {
            pe.printStackTrace();
            expiration = null;
          }
        }
        else
        {
         if(line.matches(".*Cache-Control:.*max-age=.*"))
          {
            maxage = Integer.parseInt(line.split("max-age=")[1].split(",")[0]) + (System.currentTimeMillis() / 1000);
          }
        }
      }
      lineCount++;
    }
    response = resp;
  }

  //This method is kind of hacky, but it makes sure that whatever gets returned in the byte[] can be evaluated as a string
  //without this we might get part of PNG in the byte[] which would cause exception when we try and parse it later
  public byte[] getHeaderBytes(byte[] resp)
  {
    byte[] header = new byte[resp.length];
    char newline = '\n';
    boolean flag = false;
    boolean done = false;
    int i = 0;
    Byte info;
    while(done == false && i < (resp.length - 2))
    {
      header[i] = resp[i];
      //this is pretty gnarly but essentially
      if(Character.compare((char) resp[i], newline) == 0)
      {
        if(Character.compare((char) resp[i+2], newline) == 0)
        {
          done = true;
        }
      }
      i++;
    }
    if(i == resp.length)
    {
      System.out.println("WARNING: getHeaderBytes could not find end of header.");
    }
    return header;
  }

  public Date getDate()
  {
    return date;
  }

  public Date getExpiration()
  {
    return expiration;
  }

  public long getMaxAge()
  {
    return maxage;
  }

  public File getResponse()
  {
    return response;
  }
}
