import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.util.*;
import java.lang.String;
import java.text.*;

public class CachingHTTPClient
{

  public static void main(String []args)
  {
    String url = args[0].substring(7); //trimming http:// from url
    int portToUse = 80;

    HashMap<String,CachedResponse> cacheMap = retrieveCacheMap(); //gets our cachemap from its file

    CachedResponse cachedResponse = cacheMap.get(url);

    if(cachedResponse != null && !(cachedResponse.getMaxAge() == -1 && cachedResponse.getExpiration() == null)) //check that we have a cached response, and make sure we can verify freshness of that response. Otherwise make new request
    {
      if(cachedResponse.getMaxAge() != -1)
      {
        if(cachedResponse.getMaxAge() > (System.currentTimeMillis() / 1000))
        {
          //found not to be stale by max-age
          serveFromCache(cacheMap.get(url).getResponse());
        }
        else
        {
          //found to be stale by max-age
          modifiedSinceRequest(portToUse, url, cacheMap);
        }
      }
      else
      {
        if(cachedResponse.getExpiration() != null)
        {
          if(cachedResponse.getExpiration().compareTo(new Date()) < 0)
          {
            //found not to be stale by expiration
            serveFromCache(cacheMap.get(url).getResponse());
          }
          else
          {
            //found to be stale by expiration date
            modifiedSinceRequest(portToUse, url, cacheMap);
          }
        }
      }
    }
    else
    {
      freshRequest(portToUse, url, cacheMap);
    }

    saveCacheMap(cacheMap); //saves our map through serialization
  }

  public static void saveCacheMap(HashMap<String,CachedResponse> cache)
  {
    try
    {
      FileOutputStream fo = new FileOutputStream("/tmp/ebt336/assignment1/cachemap.ser");
      try
      {
        ObjectOutputStream oos = new ObjectOutputStream(fo);
        oos.writeObject(cache);
        oos.close();
        fo.close();
      }
      catch(IOException e)
      {
        e.printStackTrace();
      }
    }
    catch(FileNotFoundException fnfe)
    {
      fnfe.printStackTrace();
    }
  }

  @SuppressWarnings("unchecked") public static HashMap<String,CachedResponse> retrieveCacheMap()
  {
    if(Files.exists(new File("/tmp/ebt336/assignment1/cachemap.ser").toPath()))
    {
      try
      {
        FileInputStream read = new FileInputStream("/tmp/ebt336/assignment1/cachemap.ser");
        try
        {
          HashMap<String,CachedResponse> cache = new HashMap<String,CachedResponse>();
          ObjectInputStream readmap = new ObjectInputStream(read);
          try
          {
            cache = (HashMap<String,CachedResponse>) readmap.readObject();
          }
          catch(ClassNotFoundException nfe)
          {
            System.out.println("WARNING: Serialized cache recovery failed.");
            nfe.printStackTrace();
            return new HashMap<String,CachedResponse>();
          }
          read.close();
          readmap.close();
          return cache;
        }
        catch(IOException e)
        {
          System.out.println("WARNING: Serialized cache recovery failed.");
          e.printStackTrace();
          return new HashMap<String,CachedResponse>();
        }
      }
      catch(FileNotFoundException fnfe)
      {
        fnfe.printStackTrace();
      }
      return new HashMap<String,CachedResponse>();
    }
    else
    {
      return new HashMap<String,CachedResponse>();
    }
  }

  public static boolean isModified(String header)
  {
    return !header.split("[\n]")[0].trim().matches(".*304.*");
  }

  public static String escapeUrl(String url)
  {
    return url.replaceAll("\\.","_").replaceAll("/","-");
  }

  public static void serveFromCache(File file)
  {
    try
    {
      FileInputStream in = new FileInputStream(file);

      byte[] buffer = new byte[200];
      int n = -1;

      System.out.println("***** Serving from the cache – start *****");
      System.out.println("\n");

      while((n = in.read(buffer)) != -1)
      {
        if(n > 0)
        {
          System.out.write(buffer, 0, n);
        }
      }

      System.out.println("\n");
      System.out.println("***** Serving from the cache – end *****");


      in.close();
    }
    catch(IOException e)
    {
      e.printStackTrace();
    }

  }

  public static void freshRequest(int portToUse, String url, HashMap<String,CachedResponse> cache)
  {
    try
    {
      //this section parses url into host and resource
      String[] url_list = url.split("/",2);
      String host = url_list[0];
      String resource;
      if(url_list.length > 1)
      {
        resource = "/" + url.split("/",2)[1];
      }
      else
      {
        resource = "/";
      }

      Socket client = new Socket(host, portToUse);

      //this section makes the request
      OutputStream out = client.getOutputStream();
      PrintWriter printout = new PrintWriter(out);
      printout.write("GET " + resource + " HTTP/1.1\n");
      printout.write("Host:" + host + "\n");
      printout.write("\n");
      printout.flush();

      //then we print it out
      InputStream in = client.getInputStream();
      byte[] buffer = new byte[200];
      int n = -1;


      File dirs = new File("/tmp/ebt336/assignment1/");
      dirs.mkdirs();
      File tocache = new File("/tmp/ebt336/assignment1/" + escapeUrl(url) + ".txt");
      FileOutputStream file = new FileOutputStream(tocache);

      System.out.println("***** Serving from the source – start *****");
      System.out.println("\n");

      while((n = in.read(buffer)) != -1)
      {
        if(n > 0)
        {
          System.out.write(buffer, 0, n);
          file.write(buffer, 0, n);
        }
      }

      System.out.println("\n");
      System.out.println("***** Serving from the source – end *****");

      file.close();

      in.close();

      //here we create file, using an escaped version of the url as a file name, and then create a CachedResponse object and pass it to our cache map

      cache.put(url,new CachedResponse(tocache));

    }
    catch(IOException e)
    {
      e.printStackTrace();
    }
  }

  public static void modifiedSinceRequest(int portToUse, String url, HashMap<String,CachedResponse> cache)
  {
    try
    {
      //this section parses url into host and resource
      String[] url_list = url.split("/",2);
      String host = url_list[0];
      String resource;
      if(url_list.length > 1)
      {
        resource = "/" + url.split("/",2)[1];
      }
      else
      {
        resource = "/";
      }

      Socket client = new Socket(host, portToUse);

      //this section makes the request
      DateFormat datemat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z");
      datemat.setTimeZone(TimeZone.getTimeZone("GMT")); //setting date format for if-modified-since
      OutputStream out = client.getOutputStream();
      PrintWriter printout = new PrintWriter(out);
      printout.write("GET " + resource + " HTTP/1.1\n");
      printout.write("Host:" + host + "\n");
      printout.write("If-Modified-Since:" + datemat.format(cache.get(url).getDate()) + "\n");
      printout.write("\n");
      printout.flush();

      InputStream in = client.getInputStream();
      byte[] buffer = new byte[200];
      int n = in.read(buffer);

      //isModified checks the header to see if we got a 304 response
      if(isModified(new String(buffer)))
      {
        //didn't get 304 -- serving fresh file
        File dirs = new File("/tmp/ebt336/assignment1/");
        dirs.mkdirs();
        File tocache = new File("/tmp/ebt336/assignment1/" + escapeUrl(url) + ".txt");
        FileOutputStream file = new FileOutputStream(tocache);

        System.out.println("***** Serving from the source – start *****");
        System.out.println("\n");

        do
        {
          if(n > 0)
          {
            System.out.write(buffer, 0, n);
            file.write(buffer, 0, n);
          }
        }while((n = in.read(buffer)) != -1);

        System.out.println("\n");
        System.out.println("***** Serving from the source – end *****");

        file.close();

        in.close();

        //here we create file, using an escaped version of the url as a file name, and then create a CachedResponse object and pass it to our cache map
        File resp = new File("./" + escapeUrl(url) + ".txt");

        cache.put(url,new CachedResponse(resp));
      }
      else
      {
        //returned 304 so we serve from cache
        serveFromCache(cache.get(url).getResponse());
      }

    }
    catch(IOException e)
    {
      e.printStackTrace();
    }

  }

}

